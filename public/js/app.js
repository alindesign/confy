/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/app.js":
/*!*****************************!*\
  !*** ./resources/js/app.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

var useMenu = function useMenu() {
  var token = 'is-minimized';
  var header = document.querySelector('.page__header');
  var menu = document.querySelector('#menu');
  var toggle = document.querySelector('#menu-toggle');

  if (localStorage.getItem('minimized') !== '1') {
    menu.classList.remove(token);
  }

  var isMinimized = function isMinimized() {
    return menu.classList.contains(token);
  };

  var toggleIcon = function toggleIcon() {
    return toggle.innerText = !isMinimized() ? 'arrow_back' : 'menu';
  };

  var headerPadding = function headerPadding() {
    return header.style.paddingLeft = isMinimized() ? '14px' : "".concat(menu.offsetWidth + 14, "px");
  };

  var menuOffset = function menuOffset() {
    return menu.style.transform = "translate(".concat(isMinimized() ? -1 * (menu.offsetWidth + 20) : 0, "px, 0)");
  };

  var handleToggle = function handleToggle() {
    menu.classList.toggle(token);
    localStorage.setItem('minimized', isMinimized() ? '1' : '0');
    toggleIcon();
    headerPadding();
    menuOffset();
  };

  toggleIcon();
  headerPadding();
  menuOffset();
  toggle.addEventListener('click', handleToggle);
};

var useMainHeight = function useMainHeight() {
  var handleResize = function handleResize() {
    var header = document.querySelector('.page__header');
    var main = document.querySelector('.page__main');
    var footer = document.querySelector('.page__footer');
    var menuHeader = document.querySelector('#menu header');

    if (main && header && footer) {
      main.style.minHeight = "".concat(window.innerHeight - header.offsetHeight - footer.offsetHeight, "px");
    }

    menuHeader.style.minHeight = "".concat(header.offsetHeight, "px");
  };

  window.addEventListener('resize', handleResize);
  window.addEventListener('load', handleResize);
  window.dispatchEvent(new Event('resize'));
};

var init = function init() {
  useMenu();
  useMainHeight();
};

init();

/***/ }),

/***/ "./resources/sass/normalize.scss":
/*!***************************************!*\
  !*** ./resources/sass/normalize.scss ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./resources/sass/style.scss":
/*!***********************************!*\
  !*** ./resources/sass/style.scss ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!***********************************************************************************************!*\
  !*** multi ./resources/js/app.js ./resources/sass/style.scss ./resources/sass/normalize.scss ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /home/alinus/projects/me/confy/resources/js/app.js */"./resources/js/app.js");
__webpack_require__(/*! /home/alinus/projects/me/confy/resources/sass/style.scss */"./resources/sass/style.scss");
module.exports = __webpack_require__(/*! /home/alinus/projects/me/confy/resources/sass/normalize.scss */"./resources/sass/normalize.scss");


/***/ })

/******/ });