<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conferences', function ( Blueprint $table ) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('speaker_id');
            $table->foreign('speaker_id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('CASCADE');

            $table->string('title');
            $table->string('description');
            $table->string('category');
            $table->string('duration');

            $table->string('room')->nullable();
            $table->string('address')->nullable();

            $table->boolean('live')->default(false);
            $table->timestamp('starts_at')->nullable();
            $table->timestamp('ends_at')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conferences');
    }
}
