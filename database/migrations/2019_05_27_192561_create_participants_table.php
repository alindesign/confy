<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function ( Blueprint $table ) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('conference_id');
            $table->foreign('conference_id')
                  ->references('id')
                  ->on('conferences')
                  ->onDelete('CASCADE');

            $table->string('chair')->nullable();

            $table->boolean('journalist')->default(false);
            $table->boolean('vip')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participants');
    }
}
