<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();

        $user->name     = "Dumitrana Alinus";
        $user->email    = "alinus@wooter.co";
        $user->admin    = true;
        $user->password = bcrypt("secret");

        $user->save();
    }
}
