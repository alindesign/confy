<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', [ 'as' => 'home', 'uses' => 'HomeController@home' ]);

Route::middleware('auth')->group(function () {
    Route::get('/dashboard', [ 'as' => 'dashboard', 'uses' => 'HomeController@dashboard' ]);

    Route::middleware('admin')->group(function () {
        Route::get('/admin', [ 'as' => 'admin', 'uses' => 'HomeController@admin' ]);
    });
});

Route::get('/posts/', [ 'as' => 'posts', 'uses' => 'HomeController@posts' ]);
Route::get('/posts/{id}', [ 'as' => 'post', 'uses' => 'HomeController@post' ]);

Route::get('/schedule', [ 'as' => 'schedule', 'uses' => 'HomeController@schedule' ]);
Route::get('/about', [ 'as' => 'about', 'uses' => 'HomeController@about' ]);
Route::get('/contact', [ 'as' => 'contact', 'uses' => 'HomeController@contact' ]);

Route::name('auth')->prefix('/auth')->group(function () {
    Route::get('/register', [ 'as' => '.sign-up', 'uses' => 'AuthController@createAccount' ]);
    Route::post('/register', [ 'as' => '.register', 'uses' => 'AuthController@register' ]);

    Route::get('/authenticate', [ 'as' => '.sign-in', 'uses' => 'AuthController@createAuthenticate' ]);
    Route::post('/authenticate', [ 'as' => '.authenticate', 'uses' => 'AuthController@authenticate' ]);

    Route::get('/logout', [ 'as' => '.logout', 'uses' => 'AuthController@logout' ]);
});
