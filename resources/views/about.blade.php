@extends('layout.app')
@section('page-title', 'About')

@section('body')
    <aside class="summary m-0 p-0">
        <div class="bg" style="background-position: center bottom;"></div>
        <div class="container">
            <header class="py-50">
                <h1>About</h1>
                <h3>Read about us</h3>
            </header>
        </div>
    </aside>
@endsection
