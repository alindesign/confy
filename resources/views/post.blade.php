<?php

use Illuminate\Support\Str;

$title   = ucfirst($post->title);
$content = json_decode($post->content ?? "{}", true);

$blocks = isset($content[ 'blocks' ]) ? $content[ 'blocks' ] : [];

function blockTag( $block )
{
    global $checks;

    switch ( $block[ 'type' ] ) {
        case 'paragraph':
            return "<p class=\"c-dark mb-4\">{$block[ 'data' ][ 'text' ]}</p>";
        case 'list':
            $tag   = $block[ 'data' ][ 'style' ][ 0 ] . 'l';
            $items = $block[ 'data' ][ 'items' ];

            foreach ( $items as $idx => $item ) {
                $items[ $idx ] = "<li>$item</li>";
            }

            $items = implode(PHP_EOL, $items);

            return "<$tag class=\"c-dark mb-14 pl-30\">$items</$tag>";
        case 'checklist':
            $items = $block[ 'data' ][ 'items' ];
            foreach ( $items as $idx => $item ) {
                $checked = ( $item[ 'checked' ] ? 'check_box' : 'check_box_outline_blank' );
                $text    = $item[ 'text' ];

                $items[ $idx ] = "<div class=\"d-flex h-flex-start v-center\">
    <i class=\"d-inline-flex c-primary material-icons\">{$checked}</i>
    <p class=\"d-inline-flex c-dark mb-0 pl-4\">{$text}</p>
</div>";
            }

            return implode(PHP_EOL, $items);
        case 'quote':
            $text    = $block[ 'data' ][ 'text' ];
            $caption = $block[ 'data' ][ 'caption' ];
            return "<div class=\"blockquote\"><blockquote>{$text}</blockquote><p class=\"blockquote-caption\"><i>{$caption}</i></p></div>";

        case 'delimiter':
            return '<hr />';

        case 'linkTool':
            $link = $block[ 'data' ][ 'link' ];
            return "<a href=\"{$link}\">{$link}</a>";

        case 'table':
            $data = [];

            foreach ( $block[ 'data' ][ 'content' ] as $idx => $row ) {
                $cols = [];
                foreach ( $row as $col ) {
                    $cols [] = "<td>{$col}</td>";
                }

                $cols = implode(PHP_EOL, $cols);

                if ( $idx > 0 ) {
                    $data[] = "<tr>{$cols}</tr>";
                } else {
                    $data[] = "<thead><tr>{$cols}</tr></thead><tbody>";
                }
            }

            $data = implode(PHP_EOL, $data);
            return "<table class=\"awesome-table\">{$data}</tbody></table>";

        case 'image':
            return "<img src=\"{$block['data']['url']}\" alt=\"image\" />";
        case 'heading':
            $tag = 'h' . $block[ 'data' ][ 'level' ];

            return "<$tag id=\"" . Str::slug(strip_tags($block[ 'data' ][ 'text' ])) . "\">{$block[ 'data' ][ 'text' ]}</$tag>";
        default:
            return "";
    }
}

?>
@extends('layout.app')
@section('page-title', $title)

@section('body')
    <script>console.log(@json($blocks));</script>

    <div class="container">
        <div class="awesome-box mt-30 p-0 post-content">
            <div class="toolbar d-flex h-flex-start v-center">
                <div class="d-inline-flex h-flex-start v-center f-50 pl-14">
                    <a href="{{ url()->route('posts') }}" class="button-link material-icons m-0">arrow_back</a>
                </div>
                <div class="d-inline-flex h-flex-end v-center f-50">
                    <p>Created By {{ $post->user->name }}</p>
                    <p>Posted at {{ $post->created_at->format('d, F Y H:i') }}</p>
                </div>
            </div>
            <div class="p-30">
                @foreach($blocks as $block)
                    {!! blockTag($block) !!}
                @endforeach
            </div>
        </div>
    </div>
@endsection
