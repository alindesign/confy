<?php

use App\Post;

$last = Post::latest()->first();
?>
<div class="d-inline-flex v-center h-flex-start f-33">
    <button class="button material-icons" id="menu-toggle"></button>
</div>
<div class="d-inline-flex v-center h-center f-33">
    @if(!isActive('admin') && !isActive('dashboard') && !isActive('post'))
        <a class="button-link" href="{{ url()->route('post', ['id'=> $last->id]) }}">Latest article: {!! ucfirst($last->title) !!}</a>
    @endif
</div>
<div class="d-inline-flex v-center h-flex-end f-33">
    @if(auth()->check())
        <a class="button" href="{{ route('dashboard') }}">Dashboard</a>
        <a class="button-link" href="{{ route('auth.logout') }}">Log out</a>
    @else
        <a class="button-link" href="{{ route('auth.sign-in') }}">Sign in</a>
        <a class="button" href="{{ route('auth.sign-up') }}">Sign up</a>
    @endIf
</div>
