<div class="container d-flex v-center">
    <nav class="d-inline-flex f-66 v-center">
        <a class="button-link c-white" href="/">Hotel</a>
        <a class="button-link c-white" href="/">Confy 2015</a>
        <a class="button-link c-white" href="/">Confy 2016</a>
        <a class="button-link c-white" href="/">Confy 2017</a>
    </nav>
    <div class="d-inline-flex f-33 v-center h-flex-end">
        <p>Confy &copy; {{ \Carbon\Carbon::now()->format('Y') }}</p>
    </div>
</div>
