@extends('layout.app')
@section('page-title', 'Contact')

@section('body')
    <aside class="summary m-0 p-0">
        <div class="bg" style="background-position: center bottom;"></div>
        <div class="container">
            <header class="py-50">
                <h1>Contact</h1>
                <h3>Do you have any questions? Send us a message.</h3>
            </header>
        </div>
    </aside>
@endsection
