@extends('layout.app')

@section('page-title', 'Sign up')

@section('body')
    <div class="container">
        <div class="awesome-form pt-80">
            <aside class="text d-inline-flex f-75">
                <h1 class="title">Sign up</h1>
                <p class="description">Create an user account and subscribe to our latest conferences, or
                    <a href="{{ route('auth.sign-in') }}" class="c-primary">sign in</a>.</p>
            </aside>
            <aside class="form-holder d-inline-flex h-flex-end f-25">
                <form class="form" action="{{ route('auth.register') }}" method="post" style="width: 100%;">
                    {!! csrf_field() !!}
                    <div class="form__group">
                        <label for="input-name">Your name:</label>
                        <input class="form__field" id="input-name" type="text" required name="name" />
                        @if( isset($errors) && count(getErrors($errors,'name')) > 0)
                            @foreach(getErrors($errors,'name') as $err)
                                <p class="form__field__hint error">{{ $err }}</p>
                            @endforeach
                        @endif
                    </div>

                    <div class="form__group">
                        <label for="input-email">Email:</label>
                        <input class="form__field" id="input-email" type="email" required name="email" />
                        @if( isset($errors) && count(getErrors($errors,'email')) > 0)
                            @foreach(getErrors($errors,'email') as $err)
                                <p class="form__field__hint error">{{ $err }}</p>
                            @endforeach
                        @endif
                    </div>

                    <div class="form__group">
                        <label for="input-password">Password:</label>
                        <input class="form__field" id="input-password" type="password" required name="password" />
                        @if( isset($errors) && count(getErrors($errors,'password')) > 0)
                            @foreach(getErrors($errors,'password') as $err)
                                <p class="form__field__hint error">{{ $err }}</p>
                            @endforeach
                        @endif
                    </div>

                    <div class="form__group">
                        <label for="input-password-confirmation">Password Confirmation:</label>
                        <input class="form__field" id="input-password-confirmation" type="password" required name="password_confirmation" />
                    </div>

                    <div class="form__group d-block t-right">
                        <button class="button c-white b-primary" type="submit">Sign up</button>
                    </div>
                </form>
            </aside>
        </div>
    </div>
@endsection()
