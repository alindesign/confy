@extends('layout.app')

@section('page-title', 'Sign in')

@section('body')
    <div class="container">
        <div class="awesome-form pt-80">
            <aside class="text d-inline-flex f-75">
                <h1 class="title">Sign In.</h1>
                <p class="description">Login now and manage your account, or create
                    <a href="{{ route('auth.sign-up') }}" class="c-primary">an account</a>.</p>
            </aside>
            <aside class="form-holder d-inline-flex h-flex-end f-25">
                <form class="form" action="{{ route('auth.authenticate') }}" method="post" style="width: 100%;">
                    <div class="form__group">
                        @if( isset($errors) && count(getErrors($errors,'main')) > 0)
                            @foreach(getErrors($errors,'main') as $err)
                                <p class="form__field__hint error">{{ $err }}</p>
                            @endforeach
                        @endif
                    </div>

                    <div class="form__group">
                        <label for="input-email">Email:</label>
                        <input class="form__field" id="input-email" type="email" required name="email" />
                        @if( isset($errors) && count(getErrors($errors,'email')) > 0)
                            @foreach(getErrors($errors,'email') as $err)
                                <p class="form__field__hint error">{{ $err }}</p>
                            @endforeach
                        @endif
                    </div>

                    <div class="form__group">
                        <label for="input-password">Password:</label>
                        <input class="form__field" id="input-password" type="password" required name="password" />
                        @if( isset($errors) && count(getErrors($errors,'password')) > 0)
                            @foreach(getErrors($errors,'password') as $err)
                                <p class="form__field__hint error">{{ $err }}</p>
                            @endforeach
                        @endif
                    </div>

                    <div class="form__group d-block t-right">
                        <button class="button c-white b-primary" type="submit">Sign in</button>
                    </div>
                </form>
            </aside>
        </div>
    </div>
@endsection()
