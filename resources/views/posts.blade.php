<?php

use App\Post;

$last = Post::latest()->first();
?>

@extends('layout.app')
@section('page-title', 'Latest articles')

@section('body')
    <aside class="summary m-0 p-0">
        <div class="bg" style="background-position: center bottom;"></div>
        <div class="container">
            <header class="py-50">
                <h1>Articles</h1>
                <h3>Latest: {!! ucfirst($last->title) !!}</h3>
            </header>
        </div>
    </aside>
    <div class="container posts pt-16 d-flex f-wrap">
        @foreach($posts as $post)
            <a href="{{ url()->route('post', ['id' => $post->id]) }}" class="d-inline-flex post p-14 f-50">
                <div class="awesome-box m-0">
                    <h3>{!! $post->title !!}</h3>
                    <p>{!! $post->summary() !!}</p>
                </div>
            </a>
        @endforeach
    </div>
@endsection
