@extends('layout.app')

@section('page-title', 'Admin')

@section('body')
<div id="root"></div>
@endsection

@section('scripts')
    <script src="{{ mix('js/admin.js') }}"></script>
@endsection
