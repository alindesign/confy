@extends('layout.app')

@section('page-title', 'Home')

@section('body')
    <section class="summary">
        <div class="bg"></div>
        <div class="container">
            <header>
                <h1>May 14 Tuesday - 16 Thursday</h1>
                <h3>University of Craiova</h3>
            </header>
            <main class="video">
                <iframe src="https://www.youtube.com/embed/WXYPpY_mElQ" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </main>
        </div>
    </section>

    <span style="display: block;width: 100%;height: 1px;"></span>
    <section class="short-about">
        <div class="container">
            <aside class="half">
                <h4>What you will get?</h4>
                <p>By participating on Confy you will get in touch with latest state of tech, get in touch with all "Stars"
                    of the software industry, plus a lot other things.</p>
            </aside>
            <aside class="half">
                <h4 class="white">Do you have any questions?</h4>
                <p class="white">Call us now on <a href="tel:555 555 555">(+40) 555 555 555</a></p>
            </aside>
        </div>
    </section>

    <form action="#" class="subscribe">
        <div class="container">
            <div class="field-group">
                <label for="subscribe_email_field">Subscribe for latest news.</label>
                <div class="field">
                    <input type="text" name="subscribe_email" id="subscribe_email_field" />
                    <button>Submit</button>
                </div>
            </div>
        </div>
    </form>

    <section class="visit">
        <div class="container">
            <div class="box">
                <img src="https://t-ec.bstatic.com/images/hotel/max1024x768/125/125150305.jpg" alt="Ramada" />
                <div class="info">
                    <h3>Enjoy your three learning days.</h3>
                    <p>Learn and enjoy three days on Ramada hotel, placed near the host of our conference, University of
                        Craiova.</p>
                    <div class="actions">
                        <a href="#">Read more</a>
                        <a href="#">Book now</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
