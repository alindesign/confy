@extends('layout.app')
@section('page-title', 'Schedule')

@section('body')
    <aside class="summary m-0 p-0">
        <div class="bg" style="background-position: center bottom;"></div>
        <div class="container">
            <header class="py-50">
                <h1>Schedule</h1>
                <h3>Latest: ReactConf</h3>
            </header>
        </div>
    </aside>
@endsection
