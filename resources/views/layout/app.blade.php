<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>@yield('page-title') | Confy</title>
        <link rel="stylesheet" href="{{ mix('/css/normalize.css') }}">
        @if(!request()->has('no_css'))
            <link rel="stylesheet" href="{{ mix('/css/style.css') }}">
            @yield('styles')
        @endif
        @yield('head')
    </head>
    <body>
        <div id="menu" class="is-minimized" style="transform: translate(-240px, 0px);">
            <header class="list d-flex v-center pl-14">
                <a class="button-link brand" href="{{ route('home') }}">Confy</a>
            </header>
            <nav class="list">
                <a class="list-item button-link {{ isActiveClass('home') }}" href="{{ route('home') }}">Home</a>
                @if(auth()->check())
                    @if(auth()->user()->isAdmin())
                        <a class="list-item button-link {{ isActiveClass('admin') }}" href="{{ route('admin') }}">Admin</a>
                    @endif
                    <a class="list-item button-link {{ isActiveClass('dashboard') }}" href="{{ route('dashboard') }}">Dashboard</a>
                @endif
                <a class="list-item button-link {{ isActiveClass('posts') }}" href="{{ route('posts') }}">Articles</a>
                <a class="list-item button-link {{ isActiveClass('schedule') }}" href="{{ route('schedule') }}">Schedule</a>
                <a class="list-item button-link {{ isActiveClass('about') }}" href="{{ route('about') }}">About</a>
                <a class="list-item button-link {{ isActiveClass('contact') }}" href="{{ route('contact') }}">Contact</a>

                <hr class="divider" />
                @if(auth()->check())
                    <a class="list-item button-link {{ isActiveClass('auth.logout') }}" href="{{ route('auth.logout') }}">Logout</a>
                @else
                    <a class="list-item button-link {{ isActiveClass('auth.sign-in') }}" href="{{ route('auth.sign-in') }}">Sign in</a>
                    <a class="list-item button-link {{ isActiveClass('auth.sign-up') }}" href="{{ route('auth.sign-up') }}">Sign up</a>
                @endif
            </nav>
        </div>
        <div id="page" class="page">
            <header class="page__header">
                @section('header')
                    @component('components.header')
                    @endcomponent
                @show
            </header>

            <main class="page__main">
                @yield('body')
            </main>

            <footer class="page__footer">
                @section('footer')
                    @component('components.footer')
                    @endcomponent
                @show
            </footer>
        </div>

        <script src="{{ mix('/js/app.js') }}"></script>
        @yield('scripts')
    </body>
</html>
