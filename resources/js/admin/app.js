import React        from 'react';
import { Provider } from './context';
import { Sidebar }  from './layout/Sidebar';
import { Content }  from './layout/Content';


export const App = () => {


    return (
        <Provider>
            <div className="container">
                <div className="row pt-30">
                    <Sidebar />
                    <Content />
                </div>
            </div>
        </Provider>
    );
};
