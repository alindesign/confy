import React      from 'react';
import classNames from 'classnames';

export const Icon = ( { children, className, component: Component } ) => (
    <Component className={ classNames(className, 'icon material-icons') }>{ children }</Component>
);

Icon.defaultProps = {
    component: 'i'
};
