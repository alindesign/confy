import Axios from 'axios';

export const api = Axios.create({
    baseURL: '/api'
});

api.interceptors.request.use(( config ) => {
    config.headers[ 'X-Requested-With' ] = 'XMLHttpRequest';

    return config;
});

const filterData = ( data ) => {
    if ( data && data.data && data.data.data ) {
        data.data = data.data.data;
    }

    if ( data && data.response && data.response.data && data.response.data.data ) {
        data.response.data = data.response.data.data;
    }

    return data;
};

api.interceptors.response.use(( data ) => {
    if ( data && data.data && data.data.data ) {
        data.data = data.data.data;
    }

    return data;
}, ( data ) => {
    if ( data && data.response && data.response.data && data.response.data.data ) {
        data.response.data = data.response.data.data;
    }

    return Promise.reject(data);
});
