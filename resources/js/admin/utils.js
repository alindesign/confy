import classes from 'classnames';

export const callFn = ( fn, ...args ) => {
    if ( fn && typeof fn === 'function' ) {
        return fn(...args);
    }
};

export { classes }
