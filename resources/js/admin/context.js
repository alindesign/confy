import React from 'react';

const DEFAULT_PAGE = 'main';

const initialState = {
    currentPage:     DEFAULT_PAGE,
    currentPageMeta: {},
    sidebarOffset:   {
        top:  0,
        left: 0
    }
};

const Context = React.createContext([ initialState ]);

const actions$ = {
    setSidebarOffset ( { state, payload } ) {
        let top  = 0;
        let left = 0;

        if ( typeof payload !== 'undefined' ) {
            if ( typeof payload === 'string' || typeof payload === 'number' ) {
                top  = payload;
                left = payload;
            } else if ( payload && typeof payload === 'object' ) {
                top  = payload.top || top;
                left = payload.left || left;
            }
        }

        return {
            ...state,
            sidebarOffset: {
                top,
                left
            }
        };
    },
    setCurrentPage ( { state, payload, args } ) {
        if ( !payload ) {
            payload = { current: DEFAULT_PAGE };
        } else {
            if ( typeof payload === 'string' ) {
                payload = { current: payload };
            }
        }

        const currentPage = payload && typeof payload === 'object' ? payload.current : DEFAULT_PAGE;
        localStorage.setItem('current_page', currentPage);

        console.log('app.current_page', currentPage);

        return {
            ...state,
            currentPage,
            currentPageMeta: ( args || [] )[ 0 ]
        };
    }
};

function reducer ( state, { type, payload, args } ) {
    return actions$[ type ]({
        state: { ...state },
        payload,
        type,
        args
    });
}

export const Provider = ( { children } ) => {
    const [ state, dispatch ] = React.useReducer(reducer, {
        ...initialState,
        currentPage: localStorage.getItem('current_page') || DEFAULT_PAGE
    });

    return (
        <Context.Provider value={ [
            state,
            ( type, payload, ...args ) => dispatch({
                type,
                payload,
                args
            })
        ] }>{ children }</Context.Provider>
    );
};

export { Context }
