import React       from 'react';
import { Context } from '../../context';

const PAGES = {};

const pages$ = require.context('../../pages', true, /\.js$/);
pages$.keys().forEach(( file ) => {
    const name = file.replace(/^\.\//, '')
                     .replace(/\.js$/, '');

    const Component = pages$(file).default;

    if ( Component ) {
        PAGES[ name ] = {
            Component,
            name,
            file
        };
    }
});

const NotFound = {
    Component () {
        return (
            <div className="d-inline-flex f-100 v-center h-center">
                <h3>Page Not Found!</h3>
            </div>
        );
    }
};

console.log(PAGES);

export const Content = () => {
    const [ state ] = React.useContext(Context);

    const { currentPage } = state;
    const { Component }   = PAGES[ currentPage ] || PAGES[ `${ currentPage }/index` ] || NotFound;

    return (
        <div className="content d-inline-flex f-wrap px-14 f-80">
            <Component />
        </div>
    );
};
