import React       from 'react';
import { classes } from '../utils';

export const Center = ( { className, ...rest } ) => (
    <div className={ classes("d-flex v-center h-center", className) } { ...rest } />
);
