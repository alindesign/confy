import React           from 'react';
import { SidebarItem } from './sidebarItem';

export const Sidebar = () => {
    return (
        <div className="sidebar f-20 pr-14">
            <nav className="awesome-box py-14 px-0 list">
                <SidebarItem name="main" icon={ 'dashboard' }>Dashboard</SidebarItem>
                <SidebarItem name="users" icon={ 'people' }>Users</SidebarItem>
                <SidebarItem name="participants" icon={ 'face' }>Participants</SidebarItem>
                <SidebarItem name="conferences" icon={ 'cast' }>Conferences</SidebarItem>
                <SidebarItem name="posts" icon={ 'collections_bookmark' }>Posts</SidebarItem>
            </nav>
        </div>
    );
};
