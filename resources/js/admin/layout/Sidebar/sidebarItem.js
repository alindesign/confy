import React       from 'react';
import classNames  from 'classnames';
import { Context } from '../../context';
import { Icon }    from '../../Icon';

export function SidebarItem ( props ) {
    let { icon, name, children } = props;
    const [ state, dispatch ]    = React.useContext(Context);

    const classes = ( page ) => classNames('list-item d-flex px-14 v-center h-flex-start button-link', {
        active: state.currentPage.startsWith(page)
    });

    const handleSetPage = ( e ) => {
        e.preventDefault();
        dispatch('setCurrentPage', name);
        return false;
    };

    return (
        <a className={ classes(name) } name={ name } href={ '#' } onClick={ handleSetPage }>
            <Icon className={ 'd-inline-flex' }>{ icon }</Icon>
            <span className={ 'd-inline-flex pl-14' }>{ children }</span>
        </a>
    );
}
