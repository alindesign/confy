import React, { Fragment } from 'react';
import { Context }         from '../context';

export const Layout = ( { title, description, createResource, children } ) => {
    const [ _, dispatch ] = React.useContext(Context);
    const ref             = React.createRef();

    React.useEffect(() => {
        document.title = `${ title } | Confy`;

        // .content.padding-top: 14px
        // .box.margin-bottom: 30px
        dispatch('setSidebarOffset', { top: ref.current.offsetHeight + 14 + 30 });
    }, [
        title,
        ref.current
    ]);

    return (
        <Fragment>
            <div className="awesome-box" ref={ ref }>
                <div className="d-flex v-flex-end h-flex-start h-flex-end">
                    <div className={ `d-inline-flex fd-column h-center v-flex-start f-${ createResource ? 66 : 100 }` }>
                        <h1>{ title }</h1>
                        <p>{ description }</p>
                    </div>
                    { ( createResource ) ? (
                        <div className="d-inline-flex v-flex-end h-flex-end f-33">
                            { createResource }
                        </div>
                    ) : null }
                </div>
            </div>
            { children }
        </Fragment>
    );
};
