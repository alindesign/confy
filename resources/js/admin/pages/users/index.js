import {
    Field,
    Form,
    Formik
} from 'formik';

import React, { Fragment } from 'react';
import { Button }          from '../../button';
import { api }             from '../../http';
import { Layout }          from '../../layout/Layout';
import { Center }          from '../../layout/Center';

import {
    Dialog,
    useDialog
} from '../../Dialog';

const initialUser = {
    name:                  '',
    email:                 '',
    password:              '',
    password_confirmation: ''
};

const UsersView = () => {
    const createEdit = useDialog();

    const [ activeUser, setActiveUser ]   = React.useState(initialUser);
    const [ data, setData ]               = React.useState([]);
    const [ error, setError ]             = React.useState(null);
    const [ loading, setLoading ]         = React.useState(false);
    const [ userLoading, setUserLoading ] = React.useState(false);
    const [ reload, setReload ]           = React.useState(new Date());

    React.useEffect(() => {
        setLoading(true);
        api.get('users')
           .then(( { data } ) => {
               setData(data);
               console.log(data);
               setLoading(false);
           }, ( e ) => {
               setError(e);
               setLoading(false);
           });
    }, [ reload ]);

    const handleEdit = ( user ) => () => {
        setActiveUser(user);
        createEdit.handleShow();
    };

    const handleDelete = ( user ) => async () => {
        setUserLoading(true);

        try {
            await api.delete(`users/${ user.id }`);
            setData([ ...data ].filter(( item ) => item.id !== user.id));
        } catch ( e ) {
            const { response } = e;
            alert(response.data.error || 'User could not be removed!');
        }

        setUserLoading(false);
    };

    const renderContent = () => {
        if ( loading ) {
            return (
                <Center>
                    <h3>Loading...</h3>
                </Center>
            );
        }

        if ( error ) {
            return (
                <Center>
                    <h3 className={ 'c-error' }>Something went wrong, users couldn't be loaded.</h3>
                </Center>
            );
        }

        if ( !data || data.length === 0 ) {
            return (
                <Center>
                    <h3>No users found ...</h3>
                </Center>
            );
        }

        return (
            <table className={ 'awesome-table' }>
                <thead>
                    <tr>
                        <th className={ 'c' }>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Admin</th>
                        <th>Register Date</th>
                        <th className={ 'right' } />
                    </tr>
                </thead>
                <tbody>{
                    data.map(( user ) => (
                        <tr key={ user.id }>
                            <td className={ 'c' }>
                                <p>{ user.id }</p>
                            </td>
                            <td>
                                <p>{ user.name }</p>
                            </td>
                            <td>
                                <p>{ user.email }</p>
                            </td>
                            <td>
                                <p>{ user.admin === 1 ? 'Yes' : 'No' }</p>
                            </td>
                            <td>
                                <p>{ user.created_at }</p>
                            </td>
                            <td className={ 'right' }>
                                <Button onClick={ handleEdit(user) } icon={ 'edit' } />
                                <Button onClick={ handleDelete(user) } icon={ 'delete' } />
                            </td>
                        </tr>
                    ))
                }</tbody>
            </table>
        );
    };

    const handleCreateEditUser = async ( state ) => {
        setUserLoading(true);

        try {
            const res = await api[ activeUser.id ? 'put' : 'post' ](`users${ activeUser.id ? `/${ activeUser.id }` : '' }`, state);

            if ( activeUser.id ) {
                setData([ ...data ].map(( user ) => user.id === res.data.id ? res.data : user));
            } else {
                setData([
                    res.data,
                    ...data
                ]);
            }

        } catch ( e ) {
            const { response } = e;
            const messages     = Object.keys(response.data.error).reduce(( bag, err ) => {
                err = response.data.error[ err ];

                return ( [
                    ...bag,
                    err[ 0 ] || err
                ] );
            }, []);

            alert(messages[ 0 ] || 'Something went wrong! ...');
        }

        setUserLoading(false);
        setActiveUser({ ...initialUser });
        createEdit.handleHide();
    };

    return (
        <Layout title={ 'Users' } description={ 'Search, Create, Edit or Delete users.' } createResource={ (
            <Button className={ 'm-0' } onClick={ createEdit.handleShow }>Create user</Button>
        ) }>
            <Dialog open={ createEdit }>
                <Formik initialValues={ activeUser } onSubmit={ handleCreateEditUser }>{ () => (
                    <Form className={ 'form pt-20 pb-14 px-28' } style={ { width: 400 } }>
                        <div className="form__group pb-14">
                            <h2 className={ 't-center' }>{ activeUser.id ? 'Edit' : 'Create' } user</h2>
                        </div>

                        { ( userLoading ) ? (
                            <Center className={ 'pb-20' }>
                                <p>Loading ...</p>
                            </Center>
                        ) : (
                            <Fragment>
                                <div className="form__group">
                                    <label htmlFor="input-name">Name:</label>
                                    <Field className="form__field" id="input-name" type="text" required name="name" />
                                </div>

                                <div className="form__group">
                                    <label htmlFor="input-email">Email:</label>
                                    <Field className="form__field" id="input-email" type="email" required name="email" />
                                </div>

                                { ( !activeUser.id ) ? (
                                    <Fragment>
                                        <div className="form__group">
                                            <label htmlFor="input-password">Password:</label>
                                            <Field className="form__field" id="input-password" type="password" required name="password" />
                                        </div>

                                        <div className="form__group">
                                            <label htmlFor="input-cpassword">Confirm Password:</label>
                                            <Field className="form__field" id="input-cpassword" type="password" required name="password_confirmation" />
                                        </div>
                                    </Fragment>
                                ) : null }

                                <div className="form__group d-inline-flex h-flex-end pt-14">
                                    <button className="button d-inline-block" type="submit">Save</button>
                                </div>
                            </Fragment>
                        ) }
                    </Form>
                ) }</Formik>
            </Dialog>

            { renderContent() }
        </Layout>
    );
};

export default UsersView;
