import React       from 'react';
import { Context } from '../../context';

const MainView = () => {
    const [ state, dispatch ] = React.useContext(Context);

    // remove when dashboard it's done
    dispatch('setCurrentPage', 'users');

    return null;
};

export default MainView;
