import React          from 'react';
import { Context }    from '../../context';
import { Layout }     from '../../layout/Layout';
import { EditorPost } from './EditorPost';

const PostsView = () => {
    const [ state, dispatch ] = React.useContext(Context);
    const { post }            = state.currentPageMeta;

    if ( !post ) {
        dispatch('setCurrentPage', 'posts');
        return null;
    }

    return (
        <Layout title={ 'Edit post' } description={ `Edit post '${ post.title }'.` }>
            <EditorPost post={ post } />
        </Layout>
    );
};

export default PostsView;
