import React       from 'react';
import { Layout }  from '../../layout/Layout';
import { EditorPost } from './EditorPost';


const PostsView = () => {
    return (
        <Layout title={ 'New post' } description={ 'Create a new post.' }>
            <EditorPost />
        </Layout>
    );
};

export default PostsView;
