import React       from 'react';
import { Button }  from '../../button';
import { Context } from '../../context';
import { api }     from '../../http';
import { Layout }  from '../../layout/Layout';
import { Center }  from '../../layout/Center';

const getSummary = ( content, title ) => {
    try {
        content = JSON.parse(content);

        const el = document.createElement('p');

        content = content.blocks
                         .filter(( block ) => (
                             /^(heading|paragraph)$/i.test(block.type) && block.data.text !== title
                         ))
                         .map(( block ) => ( block.data.text ))
                         .join(' ')
                         .trim();

        if ( content.length > 200 ) {
            content = content.substr(0, 200) + '...';
        }

        if ( !content ) {
            return (
                <p className="c-error">No content!</p>
            );
        }

        el.innerHTML = content;
        content      = el.innerText;

        return (
            <p className="c-dark" dangerouslySetInnerHTML={ { __html: content } } />
        );
    } catch ( e ) {
        return (
            <p className="c-error">Invalid content!</p>
        );
    }
};

const PostsView = () => {
    const [ state, dispatch ] = React.useContext(Context);

    const [ data, setData ]       = React.useState([]);
    const [ error, setError ]     = React.useState(null);
    const [ loading, setLoading ] = React.useState(false);

    React.useEffect(() => {
        setLoading(true);
        api.get('posts')
           .then(( { data } ) => {
               setData(data);
               setLoading(false);
           }, ( e ) => {
               setError(e);
               setLoading(false);
           });
    }, []);

    const handleNewPost = () => {
        dispatch('setCurrentPage', 'posts/new');
    };

    const handleEdit = ( post ) => () => {
        dispatch('setCurrentPage', 'posts/edit', {
            post
        });
    };

    const handleDelete = ( post ) => async () => {
        setLoading(true);

        try {
            await api.delete(`posts/${ post.id }`);

            setData([ ...( data.filter(( post$ ) => ( post$.id !== post.id )) ) ]);
        } catch ( e ) {
            const { response } = e;
            alert(response.data.error);
        }

        setLoading(false);
    };

    const renderContent = () => {
        if ( loading ) {
            return (
                <Center>
                    <h3>Loading...</h3>
                </Center>
            );
        }

        if ( error ) {
            return (
                <Center>
                    <h3 className={ 'c-error' }>Something went wrong, posts couldn't be loaded.</h3>
                </Center>
            );
        }

        if ( !data || data.length === 0 ) {
            return (
                <Center>
                    <h3>No posts found ...</h3>
                </Center>
            );
        }

        return data.map(( post ) => (
            <div className="awesome-box py-14 px-24" key={ post.id }>
                <h1 dangerouslySetInnerHTML={ { __html: `#${ post.id }. ${ post.title }` } } />
                <h4 className={ 'c-secondary mb-14' }>Author: { post.user.name }</h4>
                { getSummary(post.content, post.title) }
                <div className="d-flex v-center h-flex-end pt-24">
                    <Button href={ `/posts/${ post.id }` } component={ 'a' } className={ 'ml-0' } target={ '__blank' }>View</Button>
                    <Button onClick={ handleDelete(post) }>Delete</Button>
                    <Button onClick={ handleEdit(post) }>Edit</Button>
                </div>
            </div>
        ));
    };

    return (
        <Layout title={ 'Posts' } description={ 'Search, Create, Edit or Delete posts.' } createResource={ (
            <Button className={ 'm-0' } onClick={ handleNewPost }>New Post</Button>
        ) }>
            { renderContent() }
        </Layout>
    );
};

export default PostsView;
