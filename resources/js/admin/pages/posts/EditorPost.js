import Checklist   from '@editorjs/checklist';
import Delimiter   from '@editorjs/delimiter';
import EditorJS    from '@editorjs/editorjs';
import Embed       from '@editorjs/embed';
import Header      from '@editorjs/header';
import LinkTool    from '@editorjs/link';
import List        from '@editorjs/list';
import Marker      from '@editorjs/marker';
import Paragraph   from '@editorjs/paragraph';
import Quote       from '@editorjs/quote';
import SimpleImage from '@editorjs/simple-image';
import Table       from '@editorjs/table';
import React       from 'react';
import { Button }  from '../../button';
import { Context } from '../../context';
import { api }     from '../../http';

export const EditorPost = ( { post } ) => {
    const [ state, dispatch ] = React.useContext(Context);

    const [ loading, setLoading ] = React.useState(false);
    const [ ready, setReady ]     = React.useState(false);
    const [ editor, setEditor ]   = React.useState(null);

    const isSave = !post || !post.id;
    const isEdit = post && post.id;

    React.useEffect(() => {
        setReady(false);

        const editor = new EditorJS({
            /**
             * Create a holder for the Editor and pass its ID
             */
            holder: 'editor',

            /**
             * Available Tools list.
             * Pass Tool's class or Settings object for each Tool you want to use
             */
            tools:     {
                /**
                 * Each Tool is a Plugin. Pass them via 'class' option with necessary settings {@link docs/tools.md}
                 */
                heading: {
                    class:         Header,
                    inlineToolbar: [ 'link' ],
                    config:        {
                        placeholder: 'Header'
                    },
                    shortcut:      'CMD+SHIFT+H'
                },
                /**
                 * Or pass class directly without any configuration
                 */
                image:   {
                    class:         SimpleImage,
                    inlineToolbar: [ 'link' ]
                },

                paragraph: {
                    class:         Paragraph,
                    inlineToolbar: true
                },
                list:      {
                    class:         List,
                    inlineToolbar: true,
                    shortcut:      'CMD+SHIFT+L'
                },
                checklist: {
                    class:         Checklist,
                    inlineToolbar: true
                },
                quote:     {
                    class:         Quote,
                    inlineToolbar: true,
                    config:        {
                        quotePlaceholder:   'Enter a quote',
                        captionPlaceholder: 'Quote\'s author'
                    },
                    shortcut:      'CMD+SHIFT+O'
                },
                marker:    {
                    class:    Marker,
                    shortcut: 'CMD+SHIFT+M'
                },
                delimiter: Delimiter,
                linkTool:  LinkTool,
                embed:     Embed,
                table:     {
                    class:         Table,
                    inlineToolbar: true,
                    shortcut:      'CMD+ALT+T'
                }
            },
            data:      post && post.content ? JSON.parse(post.content) : {
                blocks: [
                    {
                        type: "heading",
                        data: {
                            text:  "Post Title",
                            level: 1
                        }
                    }
                ]
            },
            autofocus: true,
            onReady () {
                setReady(true);
            }
        });

        setEditor(editor);
    }, []);

    const handleSave = async () => {
        setLoading(true);

        const content    = await editor.save();
        const titleBlock = content.blocks.filter(( { type } ) => ( type === 'heading' ))
                                  .sort(( a, b ) => ( a.data.level - b.data.level ))[ 0 ];

        let title = 'Untitled Post';

        if ( !titleBlock ) {
            alert(`No heading specified, Post will be saved with title '${ title }'.`);
        } else {
            title = titleBlock.data.text;
        }

        try {
            await api[ isSave ? 'post' : 'put' ](`posts${ isSave ? '' : `/${ post.id }` }`, {
                title,
                content
            });

            dispatch('setCurrentPage', 'posts');
            return;
        } catch ( e ) {
            alert('Post couldn\'t be saved!');
        }

        setLoading(false);
    };

    return (
        <div className="awesome-box post-content">
            <div id={ 'editor' } />
            <div className="d-flex v-center h-flex-end">
                { ( loading ) ? ( <p className={ 'pr-8' }>Saving...</p> ) : null }
                <Button onClick={ handleSave } disabled={ !ready || loading }>Save</Button>
            </div>
        </div>
    );
};
