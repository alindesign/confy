import React      from 'react';
import classNames from 'classnames';
import { Icon }   from './Icon';

export const Button = ( { className, icon, component: Component, ...rest } ) => {
    if ( typeof icon === 'string' && !rest.children ) {
        rest.children = (
            <Icon>{ icon }</Icon>
        );
    }
    return (
        <Component className={ classNames('button', {
            'button--icon': !!icon
        }, className) } { ...rest } />
    )
};

Button.defaultProps = {
    component: 'button'
};
