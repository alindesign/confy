import React            from 'react';
import classNames       from 'classnames';
import { createPortal } from 'react-dom';
import { Button }       from './button';
import { Icon }         from './Icon';

import { callFn } from './utils'

export function useDialog ( state = false ) {
    const [ open, setOpen ] = React.useState(state);

    return {
        __hook:     true,
        open,
        setOpen,
        handleShow: () => setOpen(true),
        handleHide: () => setOpen(false)
    }
}


export const Dialog = ( { open, children, onClose, ...rest } ) => {
    let open$   = open;
    let useHook = false;

    if ( open$ && typeof open$ === 'object' && open$.__hook ) {
        open$   = open$.open;
        useHook = true;
    }

    const handleClose = () => {
        if ( useHook ) {
            open.handleHide();
        }
        callFn(onClose);
    };

    rest.className = classNames('dialog', rest.className);

    return createPortal(
        open$ ? (
            <div { ...rest }>
                <div className="dialog__backdrop" onClick={ handleClose } />
                <div className="dialog__box awesome-box">
                    <Button className="dialog__close button--icon" onClick={ handleClose }>
                        <Icon>close</Icon>
                    </Button>
                    { children }
                </div>
            </div>
        ) : false,
        document.body
    );
};
