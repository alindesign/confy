const useMenu = () => {
    const token  = 'is-minimized';
    const header = document.querySelector('.page__header');
    const menu   = document.querySelector('#menu');
    const toggle = document.querySelector('#menu-toggle');

    if ( localStorage.getItem('minimized') !== '1' ) {
        menu.classList.remove(token);
    }

    const isMinimized = () => menu.classList.contains(token);

    const toggleIcon    = () => toggle.innerText = ( !isMinimized() ? 'arrow_back' : 'menu' );
    const headerPadding = () => header.style.paddingLeft = isMinimized() ? '14px' : `${ menu.offsetWidth + 14 }px`;
    const menuOffset    = () => menu.style.transform = `translate(${ isMinimized() ? ( -1 * ( menu.offsetWidth + 20 ) ) : 0 }px, 0)`;

    const handleToggle = () => {
        menu.classList.toggle(token);
        localStorage.setItem('minimized', isMinimized() ? '1' : '0');
        toggleIcon();
        headerPadding();
        menuOffset();
    };

    toggleIcon();
    headerPadding();
    menuOffset();

    toggle.addEventListener('click', handleToggle);
};

const useMainHeight = () => {
    const handleResize = () => {
        const header = document.querySelector('.page__header');
        const main   = document.querySelector('.page__main');
        const footer = document.querySelector('.page__footer');

        const menuHeader = document.querySelector('#menu header');

        if ( main && header && footer ) {
            main.style.minHeight = `${ window.innerHeight - header.offsetHeight - footer.offsetHeight }px`;
        }

        menuHeader.style.minHeight = `${ header.offsetHeight }px`;
    };

    window.addEventListener('resize', handleResize);
    window.addEventListener('load', handleResize);
    window.dispatchEvent(new Event('resize'));
};

const init = () => {
    useMenu();
    useMainHeight();
};

init();
