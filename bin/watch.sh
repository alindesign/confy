#!/usr/bin/env bash
yarn node-sass --watch --recursive --output public/css --source-map true --source-map-contents resources/styles &
