<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * @property mixed content
 * @property mixed title
 * @property int   user_id
 */
class Post extends Model
{
    protected $table    = 'posts';
    protected $fillable = [ 'title', 'content', 'user_id' ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function summary()
    {
        $content = '';
        foreach ( $this->blocks() as $block ) {
            switch ( $block[ 'type' ] ) {
                case 'heading':
                case 'paragraph':
                    $content .= $block[ 'data' ][ 'text' ] . " ";
                    break;
            }
        }

        return Str::words(strip_tags(trim($content)), 24);
    }

    public function blocks()
    {
        $content = json_decode($this->content, true);

        return $content[ 'blocks' ] ?? [];
    }
}
