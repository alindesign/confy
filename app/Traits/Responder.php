<?php

namespace App\Traits;

use Illuminate\Http\Response;

trait Responder
{

    public function respond( $data, $status = Response::HTTP_OK, $headers = [] )
    {
        return \response()->json([ 'data' => $data ], $status, $headers);
    }

    public function respondError( $error, $status = Response::HTTP_UNPROCESSABLE_ENTITY, $headers = [] )
    {
        return $this->respond([
            'error' => $error
        ], $status, $headers);
    }
}
