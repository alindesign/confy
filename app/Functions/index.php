<?php

function getErrors( $errors, $name )
{
    $errors = is_array($errors) ? $errors : $errors->messages();

    if ( isset($errors) && isset($errors[ $name ]) && count($errors[ $name ]) > 0 ) {
        return $errors[ $name ];
    }

    return [];
}

function isActive( $name )
{
    return request()->route()->getName() === $name;
}

function isActiveClass( $name )
{
    return isActive($name) ? 'active' : '';
}
