<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Ajax extends Middleware
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param Closure                  $next
     * @param mixed                    ...$guards
     * @return mixed|string
     */
    public function handle( $request, Closure $next, ...$guards )
    {
        if ( !$request->expectsJson() ) {
            return abort(403);
        }

        return $next($request);
    }
}
