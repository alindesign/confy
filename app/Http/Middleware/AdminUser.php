<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class AdminUser extends Middleware
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param Closure                  $next
     * @param mixed                    ...$guards
     * @return mixed|string
     */
    public function handle( $request, Closure $next, ...$guards )
    {
        if ( !auth()->check() || !auth()->user()->isAdmin() ) {
            return redirect()->route('home');
        }

        return $next($request);
    }
}
