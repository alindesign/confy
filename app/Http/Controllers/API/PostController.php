<?php

namespace App\Http\Controllers\API;

use App\Traits\Responder;
use App\Post;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class PostController extends Controller
{
    use Responder;

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index( Request $request )
    {
        $posts = Post::with('user')->orderBy('created_at', 'desc');

        if ( $request->has('last') ) {
            $posts = $posts->first();
        } else {
            // TODO: add pagination & filter support (when app get complex)
            $posts = $posts->get();
        }

        return $this->respond($posts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store( Request $request )
    {
        $post = new Post();

        $post->user_id = auth()->user()->id;
        $post->title   = $request->get('title');
        $post->content = is_array($request->get('content')) ? json_encode($request->get('content')) : $request->get('content');

        $post->save();

        return $this->respond($post);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show( $id )
    {
        // Todo: implement html render filter
        return $this->respond(Post::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int     $id
     * @return JsonResponse
     */
    public function update( Request $request, $id )
    {
        $post = Post::find($id);
        $user = auth()->user();

        if ( !$post ) {
            return $this->respondError('Post not found!', Response::HTTP_NOT_FOUND);
        }

        if ( !$user->isAdmin() || $post->user_id !== $user->id ) {
            return $this->respondError('Access Forbidden', Response::HTTP_FORBIDDEN);
        }

        $post->title   = $request->get('title');
        $post->content = is_array($request->get('content')) ? json_encode($request->get('content')) : $request->get('content');

        $post->save();

        return $this->respond($post);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy( $id )
    {
        $post = Post::find($id);
        $user = auth()->user();

        if ( !$post ) {
            return $this->respondError('Post not found!', Response::HTTP_NOT_FOUND);
        }

        if ( !$user->isAdmin() || $post->user_id !== $user->id ) {
            return $this->respondError('Access Forbidden', Response::HTTP_FORBIDDEN);
        }

        return $this->respond([
            'deleted' => $post->delete(),
            'post'    => $post
        ]);
    }
}
