<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AuthController;
use App\Traits\Responder;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    use Responder;

    /**
     * @return JsonResponse
     */
    public function index()
    {
        // TODO: add pagination & filter support (when app get complex)
        return $this->respond(User::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store( Request $request )
    {
        $res = AuthController::createAccountCommand($request);
        if ( isset($res[ 'errors' ]) ) {
            /** @noinspection PhpUndefinedMethodInspection */
            return $this->respondError($res[ 'errors' ]->messages());
        }

        return $this->respond($res[ 'user' ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show( $id )
    {
        // Todo: implement privacy filter
        return $this->respond(User::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int     $id
     * @return JsonResponse
     */
    public function update( Request $request, $id )
    {
        $user = User::find($id);

        if ( !$user ) {
            return $this->respondError('User not found!', Response::HTTP_NOT_FOUND);
        }

        $user->name  = $request->input('name');
        $user->email = $request->input('email');

        if ( !$user->isAdmin() ) {
            $user->password = Hash::make($request->input('password'));
        }

        $user->save();

        return $this->respond($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy( $id )
    {
        $user = User::find($id);

        if ( !$user ) {
            return $this->respondError('User not found!', Response::HTTP_NOT_FOUND);
        }

        if ( !auth()->user()->isAdmin() ) {
            return $this->respondError('Access Forbidden', Response::HTTP_FORBIDDEN);
        }

        return $this->respond([
            'deleted' => $user->delete(),
            'user'    => $user
        ]);
    }
}
