<?php

namespace App\Http\Controllers;

use App\Post;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', [
            'only' => [ 'admin', 'dashboard' ]
        ]);

        $this->middleware('admin', [
            'only' => [ 'admin' ]
        ]);
    }

    public function home()
    {
        return view('index');
    }

    public function dashboard()
    {
        return view('dashboard');
    }

    public function post( int $id )
    {
        $post = Post::with('user')->where('id', $id)->first();

        if ( !$post ) {
            return abort(404);
        }

        return view('post', [
            'post' => $post
        ]);
    }

    public function posts()
    {
        $posts = Post::with('user')->orderBy('created_at', 'desc')->get();

        return view('posts', [
            'posts' => $posts
        ]);
    }

    public function admin()
    {
        return view('admin');
    }

    public function schedule()
    {
        return view('schedule');
    }

    public function participants()
    {
        return view('participants');
    }

    public function stuff()
    {
        return view('stuff');
    }

    public function about()
    {
        return view('about');
    }

    public function contact()
    {
        return view('contact');
    }
}
