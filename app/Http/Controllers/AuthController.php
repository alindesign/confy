<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', [
            'only' => 'createAuthenticate'
        ]);
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('auth.sign-in');
    }

    public function authenticate( Request $r )
    {
        $validator = Validator::make($r->all(), [
            'email'    => 'required|email|exists:users',
            'password' => 'required'
        ]);

        if ( $validator->fails() ) {
            return view('auth.authenticate')->withErrors($validator);
        }

        $user        = User::where('email', $r->input('email'))->first();
        $credentials = $r->only('email', 'password');

        if ( Auth::attempt($credentials) ) {
            $session = $r->session();
            $session->put('authenticated', true);
            $session->put('user', $user);

            return redirect()->route('dashboard');
        } else {
            return view('auth.authenticate', [
                'errors' => [
                    'main' => [ 'Invalid email or password!' ]
                ]
            ]);
        }
    }

    /**
     * Creating the user and store it into database
     * @param Request $r
     * @return View
     */
    public function register( Request $r )
    {
        $res = self::createAccountCommand($r);

        if ( isset($res[ 'errors' ]) ) {
            return view('auth.register')->withErrors($res[ 'errors' ]);
        }

        return redirect()->route('auth.sign-in');
    }

    public static function createAccountCommand( Request $r )
    {
        $validator = Validator::make($r->all(), [
            'name'     => 'required',
            'email'    => 'required|email|unique:users',
            'password' => 'required|confirmed'
        ]);

        if ( $validator->fails() ) {
            return [ 'errors' => $validator ];
        }

        $user = new User();

        $user->name     = $r->input('name');
        $user->email    = $r->input('email');
        $user->password = Hash::make($r->input('password'));

        $user->save();

        return [ 'user' => $user ];
    }

    /**
     * Register form page
     * @return View
     */
    public function createAccount()
    {
        return view('auth.register');
    }

    /**
     * Register form page
     * @return View
     */
    public function createAuthenticate()
    {
        return view('auth.authenticate');
    }
}
