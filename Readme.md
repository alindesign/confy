# Confy

University Web Development project.

#### Requirements
- PHP: >= 7.x
- MySQL: >= 5.7.x
- Redis: latest
- Composer: latest
- node: >= 9.x

#### Setup
```bash
$ git clone https://alindesign@bitbucket.org/alindesign/confy.git
cd confy
cp .env.example .env // Setup Database connection
composer install
php artisan key:generate
php artisan migrate
php artisan db:seed
npm install
npm dev

// Optional server setup
```

#### About
Written by Dumitrana Alinus, confy it's a management web platform for conferences written in php, using laravel as base. 
